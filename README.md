# Ladda

Пакет обертка для [ladda](https://github.com/hakimel/Ladda).

Экспортирует объект Ladda и подключает стили.

### SCSS

If you want the button styles used in the [Ladda example page](http://lab.hakim.se/ladda), import **ladda-themed.scss** file.
```scss
@import "{ladda}/ladda-themed.scss";
```

### Использование
```javascript
import Ladda from 'meteor/ladda';
```

The following approach is recommended for JavaScript control over your buttons:

```javascript
// Create a new instance of ladda for the specified button
Template.myTemplate.onRendered(function () {
  this.button = Ladda.create( this.find( '.my-button' ) );  
})
// Use this.button in helpers/events/lifecycle methods
// Start loading
this.button.start();

// Will display a progress bar for 50% of the button width
this.button.setProgress( 0.5 );

// Stop loading
this.button.stop();

// Toggle between loading/not loading states
this.button.toggle();

// Check the current state
this.button.isLoading();

// Delete the button's ladda instance
this.button.remove();
```
