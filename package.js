Package.describe({
  name: 'ladda',
  version: '0.1.1',
  summary: 'Wrapper for ladda',
  git: 'https://bitbucket.org/aspirinchaos/ladda.git',
  documentation: 'README.md',
});

Npm.depends({
  ladda: '2.0.1',
});

Package.onUse((api) => {
  api.versionsFrom('1.5');
  api.use(['ecmascript', 'fourseven:scss']);
  api.addFiles([
    'css/ladda.scss',
  ], 'client');
  api.addFiles([
    'css/ladda-themed.scss',
  ], 'client', { isImport: true });
  api.mainModule('ladda.js', 'client');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('ladda');
  api.mainModule('ladda-tests.js');
});
